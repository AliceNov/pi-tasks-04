#define _CRT_SECURE_NO_WARNINGS
#define N 256
#include<stdio.h>
int result(char str[][256], int num)
{
	int tmp[N] = { 0 };
	int i = 0, l = -1;
	while (i <= num)
	{
		for (int j = 0; j < 256; j++)
		{
			if (str[i][j] == '\0')
			{
				tmp[i] = l;
				break;
			}
			l++;
		}
		l = -1;
		i++;
	}
	printf("\n");
	l = 1;
	for (int i = 0; i < 256; i++)
		for (int k = 1; k <= num; k++)
			if (tmp[k] == i)
			{
				printf(" %d=%s", l, str[k]);
				l++;
			}
}
int main(){
	int num,i;
	char line[N][N] = { 0 };
	printf("Enter the number of lines, please\n");
	scanf("%d", &num);
	printf("\nEnter %d lines, please:\n", num);
	for (i = 0; i <= num; i++)
		fgets(line[i], 256, stdin);
	printf("\n Your string:\n\n");
	for (i = 1; i <= num; i++){
		printf(" %d = %s", i, line[i]);
	}
	printf("\n Result: \n");
	result(line, num);
	printf("\n\n");
	return 0;
}